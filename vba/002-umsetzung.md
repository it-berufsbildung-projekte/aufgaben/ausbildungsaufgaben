# VBA - 002
## Kalender Umsetzung

### Aufgabe
Es soll ein Kalender in Excel erstellt werden. Der Kalender soll mit Hilfe von VBA dynamisch erstellt werden.
Dazu soll ein Arbeitsblatt mit dem Namen Einstellungen erstellt werden. Auf diesem Arbeits-blatt muss ein Knopf vorhanden sein, welcher beim Drücken dann ein neues Excelarbeitsblatt erstellt. Dieses Arbeitsblatt muss einen klar definierten Namen haben.
Auf dem Arbeitsblatt Einstellungen werden dann allgemeine Informationen erfasst welche du später verwenden kannst:
- Das Jahr wo begonnen werden soll. Also das Jahr wo der Kalender beginnt
- Feiertage (mach dir Gedanken über Feiertage in der Schweiz, welche Typen von Fei-ertagen gibt es? Fixe und variable Feiertage und was ist der Unterschied?)
- Berufsschultage
- Betriebsferien

Der Kalender soll wie folgt aufgebaut sein:
- Eine Kopfzeile mit Überschrift und nur wo Sinnvoll angezeigt (Datum nur Mo, KW nur Mo oder bei Wochenwechsel)
  - Datum
  - KW
  - Tag (in Kurzform)
  - Berufsschul-informationen
  - Betriebsinformationen
  - Dein Name
- Pro Tag soll jetzt eine Zeile angelegt werden
- Wochenenden sollen farblich hinterlegt werden
- Feiertage sollen farblich hinterlegt werden (klar Unterscheidung zum Wochenende)
- Ferien, Berufsschultage etc. müssen in den korrekten Spalten erfasst werden und farblich hinterlegt werden. Also BFS in der Spalte BFS (Schulferien) / oder nur bei dir (Schultage). Betriebsferien etc. in den Betriebsinformationen und bei dir
- Jede Woche wird mit einer «Fetten» Linie getrennt. Die Linie geht nur über die Spalten, welche auch im wirklich benutzt werden
- Die Spalten Berufsschulinformationen, Betriebsinformationen und Dein Name wer-den mit einer «Fetten» Linie getrennt. Diese Linie kann über das ganze Arbeitsblatt gehen

Du kannst das Excel Grobplanung-alle als visuelle Vorlage verwenden um zu sehen, was in etwa gefordert ist.

![img.png](grobplanung-alle.png)

### Erwartetes Resultat
 - Excel welches einen Kalender darstellt mit der Einfärbung von Wochenenden, Feiertagen, Betriebsferien, Schulferien
 - Das Excel muss über einen Button verfügen, welcher ein neues Arbeitsblatt einfügt für ein Schuljahr vom 1.8. – 31.7 des folge Jahres. Dieses Arbeitsblatt soll den Namen Aug. Jahr – Jul. Folgejahr haben.

### Vorgehen
- Erst mal die Aufgabe verstehen. Notizen machen was gefordert ist
- Flussdiagram erstellen, um den Ablauf klar zu haben und dieses Flussdiagram abnehmen lassen
- Schritt für Schritt umsetzen und jeden Teilschritt besprechen

### Zeitbudget
- 20h, bitte die Zeiten selber einteilen um auch wirklich nach 20h fertig zu sein

### Sonstiges
- kein Web Summary notwendig


### Bewertungskriterien
- Settings Seite hat alle notwendigen Informationen und den Button. Dazu gehört auch die Defi-nition von fixen und variablen Terminen sowie Berechneten Terminen wie z.B. Ostern. Das Datum für Ostern wird korrekt berechnet.
- Die Header-zeile wird angelegt
- Die Spalten stimmen mit der Aufgabe überein
- Das Layout des Kalenders mit Spaltentrennung etc. ist korrekt
- Es wurde unterschiedliche Farben verwendet und es ist klar ersichtlich was die Farben bedeu-ten
- Einträge wie Feiertage, Ferien etc. sind korrekt gemacht
