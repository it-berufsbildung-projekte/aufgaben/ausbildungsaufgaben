# VBA - 001 Grundlage
## Grundlage

### Aufgabe
Arbeite dich in VBA ein und beantworte dazu folgende Fragen:

1. Was ist VBA
2. Welche Anwendungen Unterstützen VBA
3. Wie verwenden man VBA
4. Was ist der Unterschied zwischen VBA und Makros
5. Wo werden Makros, bzw. wird der Code gespeichert
6. Welche Möglichkeiten / Typen kann man unter VBA im Editor hinzufügen?
7. Was ist der Unterschied zwischen einem Modul und einer Klasse in VBA und wofür wird das eingesetzt
8. Nenne mindestens 2 wichtige vordefinierte Klassen in Excel

### Erwartetes Resultat
- Antworten in einem Wiki Beitrag
- Fragen zu diesem Thema in einem Fachgespräch beantworten können

### Vorgehen
- Internetrecherche
- Mit Excel Ausprobieren was hinter den Fragen steht

### Zeitbudget
- 1h

### Sonstiges
- kein Web Summary notwendig


### Bewertungskriterien
- Zu jeder Frage können Antworten gegeben werden bei einer mündlichen Abfrage durch den Auftraggeber.
- Ev. Auch vorzeigen können.
